CREATE TABLE code_store
(
    id              SERIAL    NOT NULL,
    code_store_type INTEGER,
    name            VARCHAR(128),
    code_name       VARCHAR(128),
    created_by      INTEGER,
    created_at      TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_by     INTEGER,
    modified_at     TIMESTAMP
);
CREATE UNIQUE INDEX ON code_store (id);

CREATE SEQUENCE address_id_seq INCREMENT 1 START 1;

CREATE TABLE address
(
    id                INTEGER   NOT NULL DEFAULT nextval('address_id_seq'),
    postal_code       VARCHAR(12),
    city              VARCHAR(500),
    street            VARCHAR(500),
    house_number      VARCHAR(12),
    building          VARCHAR(6),
    other_information VARCHAR(128),
    created_by        INTEGER,
    created_at        TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_by       INTEGER,
    modified_at       TIMESTAMP

);
CREATE UNIQUE INDEX ON address (id);

CREATE TABLE patient
(
    id             SERIAL    NOT NULL,
    first_name     VARCHAR(128),
    last_name      VARCHAR(128),
    mothers_name   VARCHAR(256),
    sex            INTEGER REFERENCES code_store (id),
    date_of_birth  DATE,
    date_of_death  DATE,
    place_of_birth VARCHAR(512),
    phone_number   VARCHAR(20),
    address_id     INTEGER REFERENCES address (id),
    email          VARCHAR(256),
    created_by     INTEGER,
    created_at     TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_by    INTEGER,
    modified_at    TIMESTAMP
);
CREATE UNIQUE INDEX ON patient (email);

CREATE TABLE patient_relationship
(
    first_patient_id  INTEGER,
    second_patient_id INTEGER,
    type              INTEGER REFERENCES code_store (id),
    quality           INTEGER REFERENCES code_store (id),
    close             INTEGER,
    started           DATE,
    ended             DATE,
    created_by        INTEGER,
    created_at        TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_by       INTEGER,
    modified_at       TIMESTAMP
);
CREATE UNIQUE INDEX ON patient_relationship (first_patient_id, second_patient_id);



INSERT INTO code_store (name, code_name)
VALUES ('Sex', 'sex');
INSERT INTO code_store (name, code_name)
VALUES ('Relationship type', 'relationship_type');
INSERT INTO code_store (name, code_name)
VALUES ('Relationship quality', 'relationship_quality');

INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'sex'), 'Man', 'man');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'sex'), 'Woman', 'woman');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'sex'), 'Robot', 'robot');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'sex'), 'Other', 'other');

INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_type'), 'Neighbor', 'Neighbor');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_type'), 'Brother', 'brother');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_type'), 'Sister', 'sister');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_type'), 'Colleague', 'colleague');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_type'), 'Spouse', 'spouse');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_type'), 'Other', 'other');

INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_quality'), 'Close', 'close');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_quality'), 'Bad', 'bad');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_quality'), 'Good', 'good');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_quality'), 'Neighbor', 'Neighbor');
INSERT INTO code_store (code_store_type, name, code_name)
VALUES ((SELECT id FROM code_store where code_name = 'relationship_quality'), 'Other', 'other');


INSERT INTO address (id, postal_code, city, street, house_number, building, other_information)
VALUES (1, '7700', 'Mohacs', 'Szechenyi ter', '1', 'a', null);

SELECT setval('address_id_seq', 1, true);

INSERT INTO patient (first_name, last_name, mothers_name, sex, date_of_birth, date_of_death, place_of_birth,
                     phone_number, address_id, email)
VALUES ('Nagyne Kiss', 'Virag', 'Havasi Gyoparka', (SELECT id FROM code_store where code_name = 'woman'), '2000-12-12',
        null, 'Pecs', '+36 30 867 9456', 1, 'proba.email@example.com');

INSERT INTO patient (first_name, last_name, mothers_name, sex, date_of_birth, date_of_death, place_of_birth,
                     phone_number, address_id, email)
VALUES ('Nagy', 'Istvan', 'Nagy Boglarka', (SELECT id FROM code_store where code_name = 'man'), '1999-12-12', null,
        'Pecs', '+36 30 867 9457', 1, 'proba2.email@example.com');

INSERT INTO patient_relationship (first_patient_id, second_patient_id, type, quality, close, started, ended)
VALUES ((SELECT id FROM patient WHERE email = 'proba.email@example.com'),
        (SELECT id FROM patient WHERE email = 'proba2.email@example.com'),
        (SELECT id FROM code_store WHERE code_name = 'spouse'),
        (SELECT id FROM code_store WHERE code_name = 'good'),
        1,
        '2005-10-11',
        null);

CREATE OR REPLACE FUNCTION patient_after_update_function()
    RETURNS TRIGGER AS

$$

BEGIN

    IF (NEW.date_of_death IS NOT NULL) THEN
        UPDATE patient_relationship
        SET ended = NEW.date_of_death
        WHERE (first_patient_id = NEW.id OR second_patient_id = NEW.id)
          AND (ended is null or ended > NEW.date_of_death);
    END IF;


    RETURN NEW;

END;

$$
    LANGUAGE 'plpgsql';



CREATE TRIGGER patient_after_update

    AFTER UPDATE

    ON patient

    FOR EACH ROW

EXECUTE PROCEDURE patient_after_update_function();


UPDATE patient
set date_of_death = NOW()
where id = 1;



CREATE OR REPLACE FUNCTION patient_relationship_before_insert_function()
    RETURNS TRIGGER AS

$$
DECLARE
    first_patient_dob  DATE;
    second_patient_dob DATE;
BEGIN

    SELECT date_of_birth INTO first_patient_dob FROM patient WHERE id = NEW.first_patient_id;
    SELECT date_of_birth INTO second_patient_dob FROM patient WHERE id = NEW.second_patient_id;

    if (NEW.started < first_patient_dob) THEN
        NEW.started = first_patient_dob;
    ELSIF (NEW.started < second_patient_id) THEN
        NEW.started = second_patient_dob;
    end if;

    RETURN NEW;

END;

$$
    LANGUAGE 'plpgsql';


CREATE TRIGGER patient_relationship_before_insert_trigger

    BEFORE INSERT

    ON patient_relationship

    FOR ROW

EXECUTE PROCEDURE patient_relationship_before_insert_function();

INSERT INTO patient (first_name, last_name, mothers_name, sex, date_of_birth, date_of_death, place_of_birth,
                     phone_number, address_id, email)
VALUES ('Kissne', 'Boroka', 'Havasi Gyoparka', (SELECT id FROM code_store where code_name = 'woman'), '2000-12-12',
        null, 'Pecs', '+36 30 867 9456', 1, 'proba3.email@example.com');

INSERT INTO patient (first_name, last_name, mothers_name, sex, date_of_birth, date_of_death, place_of_birth,
                     phone_number, address_id, email)
VALUES ('Egyem', 'Tamas', 'Nagy Boglarka', (SELECT id FROM code_store where code_name = 'man'), '1999-12-12', null,
        'Pecs', '+36 30 867 9457', 1, 'proba4.email@example.com');

INSERT INTO patient_relationship (first_patient_id, second_patient_id, type, quality, close, started, ended)
VALUES ((SELECT id FROM patient WHERE email = 'proba3.email@example.com'),
        (SELECT id FROM patient WHERE email = 'proba4.email@example.com'),
        (SELECT id FROM code_store WHERE code_name = 'spouse'),
        (SELECT id FROM code_store WHERE code_name = 'good'),
        1,
        '1980-10-11',
        null);

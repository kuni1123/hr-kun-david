package com.avinty.hr.controller;

import com.avinty.hr.model.Employee;
import com.avinty.hr.model.User;
import com.avinty.hr.service.EmployeeService;
import com.avinty.hr.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${application-endpoint-url}"+"/public")
@RequiredArgsConstructor
public class AuthController {

	private final UserService userService;

	@PostMapping("/register")
	public User save(@RequestBody User user){
		return this.userService.register(user);
	}
}

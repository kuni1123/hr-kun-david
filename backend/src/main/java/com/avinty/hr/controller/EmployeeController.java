package com.avinty.hr.controller;

import com.avinty.hr.constant.Permissions;
import com.avinty.hr.model.Employee;
import com.avinty.hr.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${application-endpoint-url}"+"/employees")
@RequiredArgsConstructor
public class EmployeeController {


	private final EmployeeService employeeService;

	@GetMapping()
	@PreAuthorize("hasAuthority('" + Permissions.Employee.EMPLOYEE_FIND_ALL + "')")
	public List<Employee> findAll() {
		return this.employeeService.findAll();
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.Employee.EMPLOYEE_FIND_BY_ID + "')")
	public Employee findById(@PathVariable("id") Integer id) {
		return this.employeeService.findById(id);
	}

	@PostMapping()
	@PreAuthorize("hasAuthority('" + Permissions.Employee.EMPLOYEE_SAVE + "')")
	public Employee save(@RequestBody Employee employee){
		return this.employeeService.save(employee);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.Employee.EMPLOYEE_UPDATE + "')")
	public Employee update(
			@PathVariable("id") Integer id,
			@RequestBody Employee employee){
		return this.employeeService.update(id, employee);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.Employee.EMPLOYEE_DELETE + "')")
	public void delete(@PathVariable("id") Integer id){
		this.employeeService.deleteById(id);
	}
}

package com.avinty.hr.controller;

import com.avinty.hr.constant.Permissions;
import com.avinty.hr.model.Department;
import com.avinty.hr.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${application-endpoint-url}"+"/departments")
@RequiredArgsConstructor
public class DepartmentController {

	private final DepartmentService departmentService;

	@GetMapping()
	@PreAuthorize("hasAuthority('" + Permissions.Department.DEPARTMENT_FIND_ALL + "')")
	public List<Department> findAll() {
		return this.departmentService.findAll();
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.Department.DEPARTMENT_FIND_BY_ID + "')")
	public Department findById(@PathVariable("id") Integer id) {
		return this.departmentService.findById(id);
	}

	@PostMapping()
	@PreAuthorize("hasAuthority('" + Permissions.Department.DEPARTMENT_SAVE + "')")
	public Department save(@RequestBody Department department){
		return this.departmentService.save(department);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.Department.DEPARTMENT_UPDATE + "')")
	public Department update(
			@PathVariable("id") Integer id,
			@RequestBody Department department){
		return this.departmentService.update(id, department);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.Department.DEPARTMENT_DELETE + "')")
	public void delete(@PathVariable("id") Integer id){
		this.departmentService.deleteById(id);
	}

}

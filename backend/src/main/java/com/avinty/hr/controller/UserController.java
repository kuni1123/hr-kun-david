package com.avinty.hr.controller;

import com.avinty.hr.constant.Permissions;
import com.avinty.hr.model.Employee;
import com.avinty.hr.model.User;
import com.avinty.hr.model.dto.UserProfilePicsDTO;
import com.avinty.hr.service.EmployeeService;
import com.avinty.hr.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "${application-endpoint-url}" + "/users")
public class UserController {

	private final UserService userService;

	@GetMapping()
	@PreAuthorize("hasAuthority('" + Permissions.User.USER_FIND_ALL + "')")
	public List<User> findAll() {
		return this.userService.findAll();
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.User.USER_FIND_BY_ID + "')")
	public User findById(@PathVariable("id") Integer id) {
		return this.userService.findById(id);
	}

	@PostMapping()
	@PreAuthorize("hasAuthority('" + Permissions.User.USER_SAVE + "')")
	public User save(@RequestBody User user){
		return this.userService.save(user);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.User.USER_UPDATE + "')")
	public User update(
			@PathVariable("id") Integer id,
			@RequestBody User user){
		return this.userService.update(id, user);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Permissions.User.USER_DELETE + "')")
	public void delete(@PathVariable("id") Integer id){
		this.userService.deleteById(id);
	}

	@PostMapping("/uploadProfilePics")
	@PreAuthorize("hasAuthority('" + Permissions.User.USER_PROFILE_PICTURE_UPLOAD + "')")
	public void uploadProfilePics(@RequestBody UserProfilePicsDTO dto){
		this.userService.uploadProfilePics(dto);
	}
}

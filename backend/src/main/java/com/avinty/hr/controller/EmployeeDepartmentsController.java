package com.avinty.hr.controller;

import com.avinty.hr.constant.Permissions;
import com.avinty.hr.model.EmployeeDepartment;
import com.avinty.hr.service.EmployeeDepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("${application-endpoint-url}"+"/emp-departments")
@RequiredArgsConstructor
public class EmployeeDepartmentsController {

	private final EmployeeDepartmentService employeeDepartmentService;

	@GetMapping()
	@PreAuthorize("hasAuthority('" + Permissions.User.EMP_DEPARTMENTS + "')")
	public List<EmployeeDepartment> getView(){
		return this.employeeDepartmentService.findAll();
	}

}

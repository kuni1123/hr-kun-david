package com.avinty.hr.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "permissions_id_seq_gen", sequenceName = "permissions_id_seq", allocationSize = 1)
@Table(name = "permissions", schema = "public")
public class Permission extends BaseEntity implements GrantedAuthority {

	@Column(nullable = false, unique = true)
	private String name;

	@Override
	public String getAuthority() {
		return name;
	}
}

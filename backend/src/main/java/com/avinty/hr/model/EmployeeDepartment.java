package com.avinty.hr.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;
import org.hibernate.annotations.Synchronize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Subselect( "select " +
		"row_number() over() as row_id," +
		" e.*, d.* from employees e, departments d")
@Synchronize({"employees", "departments"})
@Immutable
@Getter
@Setter
public class EmployeeDepartment {

	@Id
	@Column(name = "row_id")
	private Integer rowId;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "dep_id")
	private String depId;

	@Column(name = "name")
	private String name;

	@Column(name = "manager_id")
	private Integer managerId;
}

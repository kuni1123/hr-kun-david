package com.avinty.hr.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@SequenceGenerator(name = "employees_id_seq_gen", sequenceName = "employees_id_seq", allocationSize = 1)
@Table(name = "employees", schema = "public")
public class Employee extends BaseEntity{

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "dep_id")
	private String depId;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.DETACH , fetch = FetchType.EAGER)
	@JoinColumn(name = "dep_id", insertable = false, updatable = false)
	private Department dep;
}

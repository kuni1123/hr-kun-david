package com.avinty.hr.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserProfilePicsDTO {

	private Integer id;

	private byte[] profilePicture;
}

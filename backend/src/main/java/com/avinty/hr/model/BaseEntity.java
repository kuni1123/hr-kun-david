package com.avinty.hr.model;

import com.avinty.hr.service.UserService;
import com.avinty.hr.util.ContextProvider;
import com.avinty.hr.util.SecurityUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@CreatedDate
	@Column(name = "created_at")
	private Date createdAt;

	@CreatedBy
	@Column(name = "created_by")
	private Integer createdBy;

	@LastModifiedDate
	@Column(name = "modified_at")
	private Date modifiedAt;

	@LastModifiedBy
	@Column(name = "modified_by")
	private Integer modifiedBy;

}

package com.avinty.hr.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@SequenceGenerator(name = "employees_id_seq_gen", sequenceName = "employees_id_seq", allocationSize = 1)
@Table(name = "departments", schema = "public")
public class Department extends BaseEntity {

	@Column(name = "name")
	private String name;

	@Column(name = "manager_id")
	private Integer managerId;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "manager_id", insertable = false, updatable = false)
	private Employee manager;

}

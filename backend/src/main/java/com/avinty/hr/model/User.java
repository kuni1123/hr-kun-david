package com.avinty.hr.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@SequenceGenerator(name = "users_id_seq_gen", sequenceName = "users_id_seq", allocationSize = 1)
@Table(name = "users", schema = "public")
public class User extends BaseEntity {

	private String name;

	private String tajNumber;

	private byte[] profilePicture;

	@Column(name = "role_id")
	private Integer roleId;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id", insertable = false, updatable = false)
	private Role role;

	private String password;

	private String email;

}

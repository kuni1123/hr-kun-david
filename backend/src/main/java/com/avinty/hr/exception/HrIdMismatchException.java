package com.avinty.hr.exception;

public class HrIdMismatchException extends RuntimeException{

	public HrIdMismatchException(final String message){
		super(message);
	}

}

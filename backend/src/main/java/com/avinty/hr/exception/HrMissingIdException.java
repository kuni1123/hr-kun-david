package com.avinty.hr.exception;

public class HrMissingIdException extends RuntimeException{

	public HrMissingIdException(final String message){
		super(message);
	}

}

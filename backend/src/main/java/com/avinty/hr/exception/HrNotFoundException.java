package com.avinty.hr.exception;

public class HrNotFoundException extends RuntimeException{

	public HrNotFoundException(final String message) {
		super(message);
	}

}

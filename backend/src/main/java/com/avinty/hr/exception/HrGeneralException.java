package com.avinty.hr.exception;

public class HrGeneralException extends RuntimeException{

	public HrGeneralException(final String message) {
		super(message);
	}

}

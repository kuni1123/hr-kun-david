package com.avinty.hr.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerExceptionHandler {

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(value = HrNotFoundException.class)
	public ResponseEntity<String> handleNotFound(HrNotFoundException e){
		return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = HrGeneralException.class)
	public ResponseEntity<String> handleGeneral(HrGeneralException e){
		return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler(value = HrIdMismatchException.class)
	public ResponseEntity<String> handleMismatch(HrIdMismatchException e){
		return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = HrMissingIdException.class)
	public ResponseEntity<String> handleMissing(HrMissingIdException e){
		return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

}

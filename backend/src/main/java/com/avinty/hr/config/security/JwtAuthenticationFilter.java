package com.avinty.hr.config.security;

import com.avinty.hr.constant.ConfigComponent;
import com.avinty.hr.model.User;
import com.avinty.hr.service.UserService;
import io.jsonwebtoken.Jwts;
import com.avinty.hr.util.ContextProvider;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This filter will be used directly for user authentication. It’ll check for username and password
 * parameters from URL and calls Spring’s authentication manager to verify them.
 *
 * <p>If username and password are correct, then the filter will create a JWT token and returns it
 * in HTTP Authorization header.
 */
@Slf4j
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final ConfigComponent configComponent;

    public JwtAuthenticationFilter(
            final AuthenticationManager authenticationManager,
            final ConfigComponent configComponent) {
        this.authenticationManager = authenticationManager;
        this.configComponent = configComponent;

        this.setFilterProcessesUrl(this.configComponent.getLoginUrl());
    }

    @Override
    public Authentication attemptAuthentication(
            final HttpServletRequest request, final HttpServletResponse response) {
        final String email =
                request.getHeader("email");
        final String password = request.getHeader("password");


        final User user = ContextProvider.getBean(UserService.class).findByEmail(email);

        if (user != null) {
                final UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(user.getEmail(), password);
                return this.authenticationManager.authenticate(authenticationToken);

        } else {
            throw new UsernameNotFoundException("Authentication failed because of bad credentials: " + email);
        }
    }

    @Override
    protected void successfulAuthentication(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final FilterChain filterChain,
            final Authentication authentication) {
        final org.springframework.security.core.userdetails.User principal =
                (org.springframework.security.core.userdetails.User) authentication.getPrincipal();

        final byte[] signingKey = this.configComponent.getJwtSecret().getBytes();
        User user = ContextProvider.getBean(UserService.class).findByEmail(principal.getUsername());

        List<String> role = new ArrayList<>(List.of(user.getRole().getName()));
        role.addAll( user.getRole().getPermissions()
               .stream()
               .map(GrantedAuthority::getAuthority)
               .collect(Collectors.toList()));

        final String token =
                Jwts.builder()
                        .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
                        .setHeaderParam("typ", this.configComponent.getJwtTokenType())
                        .setIssuer(this.configComponent.getJwtTokenIssuer())
                        .setAudience(this.configComponent.getJwtTokenAudience())
                        .setSubject(principal.getUsername())
                        .claim("role", role)
                        .compact();

        response.addHeader(
                this.configComponent.getJwtTokenHeader(),
                this.configComponent.getJwtTokenPrefix() + token);
    }

}

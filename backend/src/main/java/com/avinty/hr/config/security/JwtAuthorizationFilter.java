package com.avinty.hr.config.security;


import com.avinty.hr.constant.ConfigComponent;
import com.avinty.hr.service.UserService;
import com.avinty.hr.util.ContextProvider;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.maven.surefire.shade.api.org.apache.maven.shared.utils.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final ConfigComponent configComponent;

    public JwtAuthorizationFilter(
            final AuthenticationManager authenticationManager,
            final ConfigComponent configComponent) {
        super(authenticationManager);
        this.configComponent = configComponent;
    }

    @Override
    protected void doFilterInternal(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final FilterChain filterChain)
            throws IOException, ServletException {
        final UsernamePasswordAuthenticationToken authentication = this.getAuthentication(request);
        if (authentication == null) {
            filterChain.doFilter(request, response);
            return;
        }

        final String email = authentication.getPrincipal().toString();
        authentication.setDetails(ContextProvider.getBean(UserService.class).findByEmail(email));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

    public UsernamePasswordAuthenticationToken getAuthentication(final HttpServletRequest request) {
        final String token = request.getHeader(this.configComponent.getJwtTokenHeader());
        if (StringUtils.isNotEmpty(token)
                && token.startsWith(this.configComponent.getJwtTokenPrefix())) {
            try {
                final byte[] signingKey = this.configComponent.getJwtSecret().getBytes();

                final Jws<Claims> parsedToken =
                        Jwts.parserBuilder()
                                .setSigningKey(signingKey)
                                .build()
                                .parseClaimsJws(token.replace("Bearer ", ""));

                final String username = parsedToken.getBody().getSubject();

                List<SimpleGrantedAuthority> authorities = ((List<?>) parsedToken.getBody()
                        .get("role")).stream()
                        .map(authority -> new SimpleGrantedAuthority((String) authority))
                        .collect(Collectors.toList());

                if (StringUtils.isNotEmpty(username)) {
                    return new UsernamePasswordAuthenticationToken(username, null, authorities);
                }
            } catch (final ExpiredJwtException exception) {
                log.warn(
                        "Request to parse expired JWT : {} failed : {}",
                        token,
                        exception.getMessage());
            } catch (final UnsupportedJwtException exception) {
                log.warn(
                        "Request to parse unsupported JWT : {} failed : {}",
                        token,
                        exception.getMessage());
            } catch (final MalformedJwtException exception) {
                log.warn(
                        "Request to parse invalid JWT : {} failed : {}",
                        token,
                        exception.getMessage());
            } catch (final IllegalArgumentException exception) {
                log.warn(
                        "Request to parse empty or null JWT : {} failed : {}",
                        token,
                        exception.getMessage());
            }
        }

        return null;
    }
}

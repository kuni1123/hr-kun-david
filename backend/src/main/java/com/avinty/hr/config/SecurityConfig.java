package com.avinty.hr.config;

import com.avinty.hr.config.security.JwtAuthenticationFilter;
import com.avinty.hr.config.security.JwtAuthorizationFilter;
import com.avinty.hr.config.security.SecurityService;
import com.avinty.hr.constant.ConfigComponent;
import com.avinty.hr.repository.EmployeeRepository;
import com.avinty.hr.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final UserRepository userRepository;
	private final ConfigComponent configComponent;

	@Bean
	GrantedAuthorityDefaults grantedAuthorityDefaults() {
		return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
	}
	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		return new SecurityService(this.userRepository);
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userDetailsService()).passwordEncoder(this.passwordEncoder());
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.cors()
				.and()
				.csrf()
				.disable()
				.authorizeRequests()
				.antMatchers("/api/public/**", "/api/v1/public/**", "/error/**")
				.permitAll()
				.antMatchers(this.configComponent.getEndpointUrl() + "/public/**", "/error/**")
				.permitAll()
				.antMatchers(this.configComponent.getLoginUrl())
				.permitAll()
				.antMatchers(this.configComponent.getEndpointUrl() + "/logout**")
				.permitAll()
				.antMatchers("/")
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.addFilter(
						new JwtAuthenticationFilter(
								this.authenticationManager(), this.configComponent))
				.addFilter(
						new JwtAuthorizationFilter(
								this.authenticationManager(), this.configComponent))
				.exceptionHandling()
				.accessDeniedPage("/403")
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Collections.singletonList("http://localhost:5000"));
		configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "HEAD"));
		configuration.setAllowCredentials(true);

		configuration.setAllowedHeaders(
				Arrays.asList("Authorization", "Content-Type", "email", "password", "token", "loginToken", "backoffice"));

		configuration.setExposedHeaders(Collections.singletonList("Authorization"));

		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}

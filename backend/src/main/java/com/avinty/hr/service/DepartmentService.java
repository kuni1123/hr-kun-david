package com.avinty.hr.service;

import com.avinty.hr.model.Department;

public interface DepartmentService extends BaseService<Department>{
}

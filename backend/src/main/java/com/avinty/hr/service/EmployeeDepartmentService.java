package com.avinty.hr.service;

import com.avinty.hr.model.EmployeeDepartment;

import java.util.List;

public interface EmployeeDepartmentService {

	List<EmployeeDepartment> findAll();
}

package com.avinty.hr.service;


import com.avinty.hr.model.Employee;
import com.avinty.hr.repository.EmployeeRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee, EmployeeRepository> implements EmployeeService{

	public EmployeeServiceImpl(EmployeeRepository repository) {
		super(repository);
	}

	@Override
	public Employee findByEmail(String emailAddress) {
		return this.repository.findByEmail(emailAddress);
	}

}

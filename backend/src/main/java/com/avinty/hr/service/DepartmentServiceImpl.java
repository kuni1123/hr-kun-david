package com.avinty.hr.service;

import com.avinty.hr.model.Department;
import com.avinty.hr.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl extends BaseServiceImpl<Department, DepartmentRepository> implements DepartmentService {

	public DepartmentServiceImpl(DepartmentRepository repository) {
		super(repository);
	}

}

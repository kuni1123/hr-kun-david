package com.avinty.hr.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BaseService<T> {

	T findById(Integer id);

	List<T> findAll();

	T save(T entity);

	T update(Integer id, T entity);

	void deleteById(Integer id);

}

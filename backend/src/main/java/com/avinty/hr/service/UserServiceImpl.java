package com.avinty.hr.service;

import com.avinty.hr.model.User;
import com.avinty.hr.model.dto.UserProfilePicsDTO;
import com.avinty.hr.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BaseServiceImpl<User, UserRepository> implements UserService {

	private final PasswordEncoder passwordEncoder;

	public UserServiceImpl(UserRepository repository,
						   final PasswordEncoder passwordEncoder) {
		super(repository);
		this.passwordEncoder = passwordEncoder;
	}
	@Override
	public User register(User user){
		user.setPassword(this.passwordEncoder.encode(user.getPassword()));
		return super.save(user);
	}

	@Override
	public User findByEmail(String email) {
		return this.repository.findByEmail(email);
	}

	@Override
	public void uploadProfilePics(UserProfilePicsDTO dto) {
		User user = super.findById(dto.getId());
		user.setProfilePicture(dto.getProfilePicture());
		super.update(dto.getId(), user);
	}
}

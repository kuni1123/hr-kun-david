package com.avinty.hr.service;

import com.avinty.hr.model.User;
import com.avinty.hr.model.dto.UserProfilePicsDTO;

public interface UserService extends BaseService<User>{

	User register(User user);

	User findByEmail(String email);

	void uploadProfilePics(UserProfilePicsDTO dto);
}

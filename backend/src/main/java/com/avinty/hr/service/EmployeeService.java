package com.avinty.hr.service;

import com.avinty.hr.model.Employee;

public interface EmployeeService extends BaseService<Employee>{

	Employee findByEmail(String emailAddress);

}

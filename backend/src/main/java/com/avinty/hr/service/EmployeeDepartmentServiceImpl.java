package com.avinty.hr.service;

import com.avinty.hr.model.EmployeeDepartment;
import com.avinty.hr.repository.EmployeeDepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeDepartmentServiceImpl implements EmployeeDepartmentService {

	private final EmployeeDepartmentRepository employeeDepartmentRepository;

	@Override
	public List<EmployeeDepartment> findAll() {
		return this.employeeDepartmentRepository.findAll();
	}
}

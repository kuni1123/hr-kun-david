package com.avinty.hr.service;

import com.avinty.hr.exception.*;
import com.avinty.hr.model.BaseEntity;
import com.avinty.hr.util.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
@RequiredArgsConstructor
public class BaseServiceImpl<S extends BaseEntity, U extends JpaRepository<S, Integer>> implements BaseService<S> {

	protected final U repository;

	@Override
	public S findById(Integer id) {
		return this.repository.findById(id)
				.orElseThrow(
						() -> new HrNotFoundException("Entity not found with id: " + id)
				);
	}

	@Override
	public List<S> findAll() {
		return this.repository.findAll();
	}

	@Override
	public S save(S entity) {
		entity.setCreatedAt(new Date(System.currentTimeMillis()));
		entity.setCreatedBy(SecurityUtil.getLoggedInUser().getId());
		entity.setModifiedBy(SecurityUtil.getLoggedInUser().getId());
		entity.setModifiedAt(new Date(System.currentTimeMillis()));
		return this.repository.save(entity);
	}

	@Override
	public S update(final Integer id, final S entity) {


		if (entity == null) {
			throw new HrGeneralException("Missing object!");
		}

		if (entity.getId() == null) {
			throw new HrMissingIdException("Missing id!");
		}

		if (!entity.getId().equals(id)) {
			throw new HrIdMismatchException("Id does not match with the given one!");
		}

		S old = this.findById(id);
		entity.setId(id);
		entity.setCreatedAt(old.getCreatedAt());
		entity.setCreatedBy(old.getCreatedBy());
		entity.setModifiedBy(SecurityUtil.getLoggedInUser().getId());
		entity.setModifiedAt(new Date(System.currentTimeMillis()));
		return this.repository.save(entity);
	}

	@Override
	public void deleteById(Integer id) {
		this.repository.deleteById(id);
	}
}

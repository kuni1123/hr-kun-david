package com.avinty.hr.constant;

public class Permissions {

	public static class Roles {
		public static final String ROLE_ADMIN = "ROLE_ADMIN";
		public static final String ROLE_USER = "ROLE_USER";
	}
	public static class Department {
		public static final String DEPARTMENT_FIND_ALL = "DEPARTMENT_FIND_ALL";
		public static final String DEPARTMENT_FIND_BY_ID = "DEPARTMENT_FIND_BY_ID";
		public static final String DEPARTMENT_SAVE = "DEPARTMENT_SAVE";
		public static final String DEPARTMENT_UPDATE = "DEPARTMENT_UPDATE";
		public static final String DEPARTMENT_DELETE = "DEPARTMENT_DELETE";
	}

	public static class Employee {
		public static final String EMPLOYEE_FIND_ALL = "EMPLOYEE_FIND_ALL";
		public static final String EMPLOYEE_FIND_BY_ID = "EMPLOYEE_FIND_BY_ID";
		public static final String EMPLOYEE_SAVE = "EMPLOYEE_SAVE";
		public static final String EMPLOYEE_UPDATE = "EMPLOYEE_UPDATE";
		public static final String EMPLOYEE_DELETE = "EMPLOYEE_DELETE";
	}

	public static class User {
		public static final String USER_FIND_ALL = "USER_FIND_ALL";
		public static final String USER_FIND_BY_ID = "USER_FIND_BY_ID";
		public static final String USER_SAVE = "USER_SAVE";
		public static final String USER_UPDATE = "USER_UPDATE";
		public static final String USER_DELETE = "USER_DELETE";
		public static final String USER_PROFILE_PICTURE_UPLOAD = "USER_PROFILE_PICTURE_UPLOAD";
		public static final String EMP_DEPARTMENTS = "EMP_DEPARTMENTS";
	}
}

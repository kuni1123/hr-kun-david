package com.avinty.hr.constant;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class ConfigComponent {

    // --- General --- //

    @Value("${application-endpoint-url}")
    private String endpointUrl;

    @Value("${application-login-url}")
    private String loginUrl;

    // --- JWT --- //

    @Value("${jwt-token-type}")
    private String jwtTokenType;

    @Value("${jwt-secret}")
    private String jwtSecret;

    @Value("${jwt-token-header}")
    private String jwtTokenHeader;

    @Value("${jwt-token-prefix}")
    private String jwtTokenPrefix;

    @Value("${jwt-token-issuer}")
    private String jwtTokenIssuer;

    @Value("${jwt-token-audience}")
    private String jwtTokenAudience;

}

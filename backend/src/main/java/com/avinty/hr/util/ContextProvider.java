package com.avinty.hr.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ContextProvider implements ApplicationContextAware {

    public static ApplicationContext context;

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext)
            throws BeansException {
        context = applicationContext;
    }

    /** Get a Spring bean by type. */
    public static <T> T getBean(final Class<T> beanClass) {
        return context.getBean(beanClass);
    }

    /** Get a Spring bean by name. */
    public static Object getBean(final String beanName) {
        return context.getBean(beanName);
    }
}

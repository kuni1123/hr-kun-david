package com.avinty.hr.util;

import com.avinty.hr.model.Permission;
import com.avinty.hr.model.User;
import com.avinty.hr.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


public class SecurityUtil {

    private SecurityUtil() {}

    public static Authentication getAuthentication() {
        if (SecurityContextHolder.getContext() != null) {
            return SecurityContextHolder.getContext().getAuthentication();
        }
        return null;
    }

    /**
     * Returns the username encoded from the sent token
     *
     * @return username (email address in this case)
     */
    public static String getLoggedInUserName() {
        if (getAuthentication() != null) {
            return getAuthentication().getName();
        }
        return null;
    }

    /**
     * Checks, whether the user has the required permission
     *
     * @param permission the permission to check against
     * @return true if the user has the given permission
     */
    public static Boolean hasPermission(final Integer permission) {
        final Authentication aut = SecurityUtil.getAuthentication();
        if (aut != null && aut.isAuthenticated() && aut.getPrincipal() != null) {
            final User user = ContextProvider.getBean(UserService.class).findByEmail(aut.getName());
            if (user.getRole() != null && user.getRole().getPermissions() != null) {
                for (final Permission item : user.getRole().getPermissions()) {
                    if (item.getId().equals(permission)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks, whether the user has the required role
     *
     * @param role the role to check against
     * @return true if the user has the given role
     */
    public static Boolean hasRole(final Integer role) {
        final Authentication aut = SecurityUtil.getAuthentication();
        if (aut != null && aut.isAuthenticated() && aut.getPrincipal() != null) {
            final User user = ContextProvider.getBean(UserService.class).findByEmail(aut.getName());
            if (user.getRole() != null) {
                return user.getRole().getId().equals(role);
            }
        }
        return false;
    }

    public static User getLoggedInUser() {
        final Authentication aut = SecurityUtil.getAuthentication();
        return ContextProvider.getBean(UserService.class).findByEmail(aut.getName());
    }
}

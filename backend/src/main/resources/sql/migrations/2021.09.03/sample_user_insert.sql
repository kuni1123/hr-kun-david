--liquibase formatted sql
--changeset Kun David:sample_user_insert

INSERT INTO users (id, name, taj_number, profile_picture, email, password, created_by, created_at, modified_by, modified_at, role_id)
 VALUES (2, 'Admin User', '123 456 789', null, 'admin@avinty.com', '$2a$12$qTsacqe06KsOf/UFBjj3gOzH.Pam0FF4NB36XfxMkhDh7VXBgMrmy', 1, NOW(), 1, NOW(), 1);
INSERT INTO users (id, name, taj_number, profile_picture, email, password, created_by, created_at, modified_by, modified_at, role_id)
VALUES (1, 'User User', '987 654 321', null, 'user@avinty.com', '$2a$12$qTsacqe06KsOf/UFBjj3gOzH.Pam0FF4NB36XfxMkhDh7VXBgMrmy', 1, NOW(), 1, NOW(), 2);

select setval('users_id_seq', max(id)) from users;

--liquibase formatted sql
--changeset Kun David:user_table

CREATE TABLE users
(
    id              SERIAL PRIMARY KEY,
    name            varchar(255),
    taj_number      varchar(11),
    profile_picture bytea,
    email           varchar(128) NOT NULL,
    password        varchar(128) NOT NULL,
    created_by      INTEGER,
    created_at      TIMESTAMP    NOT NULL,
    modified_by     INTEGER,
    modified_at     TIMESTAMP    NOT NULL
)

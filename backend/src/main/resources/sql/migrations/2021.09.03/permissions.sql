--liquibase formatted sql
--changeset Kun David:permissions

CREATE TABLE roles
(
    id          SERIAL PRIMARY KEY,
    name        varchar(255),
    created_by  INTEGER,
    created_at  TIMESTAMP NOT NULL,
    modified_by INTEGER,
    modified_at TIMESTAMP NOT NULL
);

CREATE TABLE permissions
(
    id          SERIAL PRIMARY KEY,
    name        varchar(255),
    created_by  INTEGER,
    created_at  TIMESTAMP NOT NULL,
    modified_by INTEGER,
    modified_at TIMESTAMP NOT NULL
);

CREATE TABLE role_x_permissions (
  role_id integer references roles (id),
  permission_id integer references permissions (id)
);

ALTER TABLE users ADD COLUMN role_id integer references roles (id);

INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('DEPARTMENT_FIND_ALL', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('DEPARTMENT_FIND_BY_ID', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('DEPARTMENT_SAVE', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('DEPARTMENT_UPDATE', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('DEPARTMENT_DELETE', NOW(), 1, NOW(), 1);

INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('EMPLOYEE_FIND_ALL', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('EMPLOYEE_FIND_BY_ID', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('EMPLOYEE_SAVE', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('EMPLOYEE_UPDATE', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('EMPLOYEE_DELETE', NOW(), 1, NOW(), 1);

INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('USER_FIND_ALL', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('USER_FIND_BY_ID', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('USER_SAVE', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('USER_UPDATE', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('USER_DELETE', NOW(), 1, NOW(), 1);

INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('USER_PROFILE_PICTURE_UPLOAD', NOW(), 1, NOW(), 1);
INSERT INTO permissions (name, created_at, created_by, modified_at, modified_by) VALUES ('EMP_DEPARTMENTS', NOW(), 1, NOW(), 1);

INSERT INTO roles (name, created_at, created_by, modified_at, modified_by)  VALUES ('ROLE_ADMIN', NOW(), 1, NOW(), 1);
INSERT INTO roles (name, created_at, created_by, modified_at, modified_by)  VALUES ('ROLE_USER', NOW(), 1, NOW(), 1);

insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'DEPARTMENT_FIND_ALL'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'DEPARTMENT_FIND_BY_ID'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'DEPARTMENT_SAVE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'DEPARTMENT_UPDATE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'DEPARTMENT_DELETE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'EMPLOYEE_FIND_ALL'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'EMPLOYEE_FIND_BY_ID'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'EMPLOYEE_SAVE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'EMPLOYEE_UPDATE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'EMPLOYEE_DELETE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'USER_FIND_ALL'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'USER_FIND_BY_ID'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'USER_SAVE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'USER_UPDATE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'USER_DELETE'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'USER_PROFILE_PICTURE_UPLOAD'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_ADMIN'), (SELECT id FROM permissions WHERE name = 'EMP_DEPARTMENTS'));

insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_USER'), (SELECT id FROM permissions WHERE name = 'USER_PROFILE_PICTURE_UPLOAD'));
insert into role_x_permissions (role_id, permission_id) VALUES ((SELECT id FROM roles WHERE name = 'ROLE_USER'), (SELECT id FROM permissions WHERE name = 'EMP_DEPARTMENTS'));


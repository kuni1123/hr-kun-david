--liquibase formatted sql
--changeset Kun David:core_sql

CREATE TABLE employees
(
    id          SERIAL PRIMARY KEY,
    email       VARCHAR(255) NOT NULL,
    password    varchar(66) NOT NULL,
    full_name   varchar(200),
    dep_id      int,
    created_by  INTEGER,
    created_at  TIMESTAMP   NOT NULL,
    modified_by INTEGER,
    modified_at TIMESTAMP   NOT NULL
);
CREATE UNIQUE INDEX ON employees (email);

CREATE TABLE departments
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(100) NOT NULL,
    manager_id  int NOT NULL,
    created_by  INTEGER,
    created_at  TIMESTAMP   NOT NULL,
    modified_by INTEGER,
    modified_at TIMESTAMP   NOT NULL
);
CREATE UNIQUE INDEX ON departments (name);

--liquibase formatted sql
--changeset Kun David:init_data

INSERT INTO employees (email,
                       password,
                       full_name,
                       dep_id,
                       created_at,
                       created_by,
                       modified_at,
                       modified_by
                       ) VALUES ('email@valami.hu', 'jelso2021', 'Dr. Teljes Nev', 1, NOW(), 1, NOW(), 1);

INSERT INTO departments (name,
                         manager_id,
                         created_at,
                         created_by,
                         modified_at,
                         modified_by) VALUES ('Department nev', 1, NOW(), 1, NOW(), 1);
